AWSTemplateFormatVersion: 2010-09-09
Description: >-
  VPC Default: VPC for the default account. Will create the vpc, 2 private subnets, 2 public subnets, and a NAT gateway.

Mappings:
  SubnetConfig:
    VPC:
      CIDR: 172.19.192.0/18
    Subnet1-Private:
      CIDR: 172.19.192.0/23
    Subnet2-Private:
      CIDR: 172.19.194.0/23
    Subnet3-Public:
      CIDR: 172.19.196.0/24
    Subnet4-Public:
      CIDR: 172.19.197.0/24

Parameters:
  AvailabilityZone1:
    Type: AWS::EC2::AvailabilityZone::Name
  AvailabilityZone2:
    Type: AWS::EC2::AvailabilityZone::Name
  pFlowLogName:
    Type: String
    Description: The name of the flow log group
    ConstraintDescription: must match pattern [A-Za-z]
    AllowedPattern: ^[a-zA-Z0-9]*$
  pFlowRoleName:
    Type: String
    Description: The name of the flow log IAM role
    ConstraintDescription: must match pattern [A-Za-z0-9]+
    AllowedPattern: ^[a-zA-Z0-9]*$

Resources:

  #-----------------------------------
  # Creates the VPC and subnets for the deployment
  #

  VPCDefault:
    Type: 'AWS::EC2::VPC'
    Properties:
      EnableDnsSupport: 'true'
      EnableDnsHostnames: 'true'
      CidrBlock: !FindInMap 
        - SubnetConfig
        - VPC
        - CIDR
      Tags:
        - Key: Application
          Value: !Ref 'AWS::StackName'
        - Key: Network
          Value: Private

  DefaultSubnet1Private:
    Type: 'AWS::EC2::Subnet'
    Properties:
      VpcId: !Ref VPCDefault
      CidrBlock: !FindInMap 
        - SubnetConfig
        - Subnet1-Private
        - CIDR
      AvailabilityZone: !Ref AvailabilityZone1
      Tags:
        - Key: Application
          Value: !Ref 'AWS::StackName'
        - Key: Network
          Value: Private

  DefaultSubnet2Private:
    Type: 'AWS::EC2::Subnet'
    Properties:
      VpcId: !Ref VPCDefault
      CidrBlock: !FindInMap 
        - SubnetConfig
        - Subnet2-Private
        - CIDR
      AvailabilityZone: !Ref AvailabilityZone2
      Tags:
        - Key: Application
          Value: !Ref 'AWS::StackName'
        - Key: Network
          Value: Private

  DefaultSubnet3Public:
    Type: 'AWS::EC2::Subnet'
    Properties:
      VpcId: !Ref VPCDefault
      CidrBlock: !FindInMap 
        - SubnetConfig
        - Subnet3-Public
        - CIDR
      AvailabilityZone: !Ref AvailabilityZone1
      Tags:
        - Key: Application
          Value: !Ref 'AWS::StackName'
        - Key: Network
          Value: Public

  DefaultSubnet4Public:
    Type: 'AWS::EC2::Subnet'
    Properties:
      VpcId: !Ref VPCDefault
      CidrBlock: !FindInMap 
        - SubnetConfig
        - Subnet4-Public
        - CIDR
      AvailabilityZone: !Ref AvailabilityZone2
      Tags:
        - Key: Application
          Value: !Ref 'AWS::StackName'
        - Key: Network
          Value: Public

  #-----------------------------------
  # Creates the internet gateway for the public subnets
  #

  InternetGateway:
    Type: 'AWS::EC2::InternetGateway'
  GatewayToInternet:
    Type: 'AWS::EC2::VPCGatewayAttachment'
    Properties:
      VpcId: !Ref VPCDefault
      InternetGatewayId: !Ref InternetGateway

  #-----------------------------------
  # This section creates two NAT gateways, one in each
  # public subnet
  #

  NATGatewayOne:
    DependsOn: GatewayToInternet
    Type: 'AWS::EC2::NatGateway'
    Properties:
      AllocationId: !GetAtt 
        - ElasticIPOne
        - AllocationId
      SubnetId: !Ref DefaultSubnet3Public
  ElasticIPOne:
    Type: 'AWS::EC2::EIP'
    Properties:
      Domain: vpc

  NATGatewayTwo:
    DependsOn: GatewayToInternet
    Type: 'AWS::EC2::NatGateway'
    Properties:
      AllocationId: !GetAtt 
        - ElasticIPTwo
        - AllocationId
      SubnetId: !Ref DefaultSubnet4Public
  ElasticIPTwo:
    Type: 'AWS::EC2::EIP'
    Properties:
      Domain: vpc

  #-------------------------------------
  # This section creates the route tables and route
  # table entries to allow internet access through
  # the NAT gateways.
  #

  PublicRouteTableOne:
    Type: 'AWS::EC2::RouteTable'
    Properties:
      VpcId: !Ref VPCDefault
  PublicRouteOne:
    Type: 'AWS::EC2::Route'
    Properties:
      RouteTableId: !Ref PublicRouteTableOne
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway
  PublicSubnetOneRouteTableAssociation:
    Type: 'AWS::EC2::SubnetRouteTableAssociation'
    Properties:
      SubnetId: !Ref DefaultSubnet3Public
      RouteTableId: !Ref PublicRouteTableOne

  PublicRouteTableTwo:
    Type: 'AWS::EC2::RouteTable'
    Properties:
      VpcId: !Ref VPCDefault
  PublicRouteTwo:
    Type: 'AWS::EC2::Route'
    Properties:
      RouteTableId: !Ref PublicRouteTableTwo
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway
  PublicSubnetTwoRouteTableAssociation:
    Type: 'AWS::EC2::SubnetRouteTableAssociation'
    Properties:
      SubnetId: !Ref DefaultSubnet4Public
      RouteTableId: !Ref PublicRouteTableTwo

  PrivateRouteTableOne:
    Type: 'AWS::EC2::RouteTable'
    Properties:
      VpcId: !Ref VPCDefault
  PrivateRouteToInternetOne:
    Type: 'AWS::EC2::Route'
    Properties:
      RouteTableId: !Ref PrivateRouteTableOne
      DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: !Ref NATGatewayOne
  PrivateSubnetOneRouteTableAssociation:
    Type: 'AWS::EC2::SubnetRouteTableAssociation'
    Properties:
      SubnetId: !Ref DefaultSubnet1Private
      RouteTableId: !Ref PrivateRouteTableOne

  PrivateRouteTableTwo:
    Type: 'AWS::EC2::RouteTable'
    Properties:
      VpcId: !Ref VPCDefault
  PrivateRouteToInternetTwo:
    Type: 'AWS::EC2::Route'
    Properties:
      RouteTableId: !Ref PrivateRouteTableTwo
      DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: !Ref NATGatewayTwo
  PrivateSubnetTwoRouteTableAssociation:
    Type: 'AWS::EC2::SubnetRouteTableAssociation'
    Properties:
      SubnetId: !Ref DefaultSubnet2Private
      RouteTableId: !Ref PrivateRouteTableTwo

  #-----------------------------------
  # This section creates the VPC flow logs
  # and associates them with the correct role
  # to publish to the logs
  #

  VPCFlowlogs:
    Type: AWS::EC2::FlowLog
    Properties:
      DeliverLogsPermissionArn:
        'Fn::GetAtt':
          - CloudWatchPubIAM
          - Arn
      LogDestinationType: cloud-watch-logs
      LogGroupName: !Ref FlowLogGroup
      ResourceId: !Ref VPCDefault
      ResourceType: VPC
      TrafficType: ALL
 
  FlowLogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Ref pFlowLogName
      RetentionInDays: 365

 # ------------------------------------
 # These are the minimum access requirements to create
 # and publish to the cloudwatch loggroup.
 #

  CloudWatchPubIAM:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Ref pFlowRoleName
      AssumeRolePolicyDocument: 
        Version: "2012-10-17"
        Statement: 
          - 
            Effect: "Allow"
            Principal: 
              Service: 
                - "vpc-flow-logs.amazonaws.com"
            Action: 
              - "sts:AssumeRole"
      Policies:
        -
          PolicyName: FlowLogWrite
          PolicyDocument: 
            Version: "2012-10-17"
            Statement:
              - 
                Effect: "Allow"
                Action:
                  - 'logs:CreateLogStream'
                  - 'logs:PutLogEvents'
                  - 'logs:DescribeLogGroups'
                  - 'logs:DescribeLogStreams'
                Resource:
                  -
                    "arn:aws:logs:*:*:*"
